package io.codezone.springboot.app.models.entity;

import java.io.Serializable;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import lombok.Getter;
import lombok.Setter;

@Table(name = "authorities", uniqueConstraints = @UniqueConstraint(columnNames = {"user_id","authority"}))
@Entity
public class Role implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id @Getter @Setter
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Getter @Setter
    private String authority;

}
