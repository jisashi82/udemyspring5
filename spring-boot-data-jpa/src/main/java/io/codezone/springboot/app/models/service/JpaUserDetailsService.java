package io.codezone.springboot.app.models.service;

import java.util.Collection;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.codezone.springboot.app.models.dao.IUsuarioDao;
import io.codezone.springboot.app.models.entity.Usuario;

@Service("jpaUserDetailsService")
public class JpaUserDetailsService implements UserDetailsService {

    @Autowired
    private IUsuarioDao usuarioDao;

    private Logger logger = LoggerFactory.getLogger(JpaUserDetailsService.class);

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Usuario usuario = usuarioDao.findByUsername(username);
        if(usuario == null) {
            logger.error("Error login: no existe el usuario: " + username);
            throw new UsernameNotFoundException("Usuario no encontrado: " + username);
        }

        /*
         * List<GrantedAuthority> authorities = new ArrayList<>();
         * for (Role role : usuario.getRoles()) {
         * authorities.add(new SimpleGrantedAuthority(role.getAuthority()));
         * }
         */
        Collection<GrantedAuthority> authorities = usuario.getRoles().stream()
                .map(role -> new SimpleGrantedAuthority(role.getAuthority()))
                .collect(Collectors.toList());

        if(authorities.isEmpty() || authorities.size() == 0) {
            logger.error("Error login: no existe ningún rol del usuario: " + username);
            throw new UsernameNotFoundException("Los Roles del usuario no pueden estar vacios: " + username);
        }

        return new User(usuario.getUsername(), usuario.getPassword(), usuario.getEnabled(), true, true, true,
                authorities);
    }

}
