package io.codezone.springboot.web.app.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/params")
public class EjemploParamsController {
	
	//http://localhost:8080/params/string?texto=HolaAbel
	@GetMapping("/string")
	public String param(@RequestParam(required = false) String texto,Model model) {
		model.addAttribute("resultado", "el valor del Parametro texto es: "+ texto);
		return "params/ver";
	}
	
	@GetMapping("/mix-params")
	public String param(@RequestParam String saludo,@RequestParam Integer numero,Model model) {
		model.addAttribute("resultado", "El Saludo es : '"+ saludo + "' y el numero es: "+numero);
		return "params/ver";
	}
	
	//utilizando la clase HttpeServletRequest para capturar los parametros, es mas verboso que las anotaciones
	@GetMapping("/mix-params-request")
	public String param(HttpServletRequest request,Model model) {
		String saludo=request.getParameter("saludo");
		Integer numero=null;
		try {
			numero= Integer.parseInt(request.getParameter("numero"));
		}catch (Exception e) {
			numero=0;
		}
		
		model.addAttribute("resultado", "El Saludo es : '"+ saludo + "' y el numero es: "+numero);
		return "params/ver";
	}
	

}
