package io.codezone.springboot.app.auth.handler;

import java.io.IOException;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.support.SessionFlashMapManager;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.FlashMap;

@Component
public class LogoutSuccessHandler  extends SimpleUrlLogoutSuccessHandler{
    
    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
            throws IOException, ServletException {
               
                SessionFlashMapManager flashMapMngr = new SessionFlashMapManager();
                FlashMap flashMap = new FlashMap();
                flashMap.put("message",  authentication.getName() + ", has cerrado sesión con éxito!");
                flashMap.put("alertClass", "alert-success");
                flashMapMngr.saveOutputFlashMap(flashMap, request, response);
        
        super.onLogoutSuccess(request, response, authentication);
    }
}
