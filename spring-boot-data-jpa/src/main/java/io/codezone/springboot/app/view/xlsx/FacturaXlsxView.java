package io.codezone.springboot.app.view.xlsx;

import java.util.Map;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractXlsxView;

import io.codezone.springboot.app.models.entity.Factura;
import io.codezone.springboot.app.models.entity.ItemFactura;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Component("factura/ver.xlsx")
public class FacturaXlsxView extends AbstractXlsxView {

	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		MessageSourceAccessor mensaje = getMessageSourceAccessor();
		
		//nombre del archivo a generar
		response.setHeader("Content-Disposition", "attachment; filename=\"factura.xlsx\"");
		
		//obtenemos la factura mediante el modelo
		Factura factura = (Factura) model.get("factura");
		//creamos la hoja
		Sheet sheet = workbook.createSheet("Factura");
		
		//creamos los estilos con CellStyle
		CellStyle theheaderStyle = workbook.createCellStyle();
		theheaderStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		theheaderStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		theheaderStyle.setBorderBottom(BorderStyle.MEDIUM);
		theheaderStyle.setBorderLeft(BorderStyle.MEDIUM);
		theheaderStyle.setBorderRight(BorderStyle.MEDIUM);
		theheaderStyle.setBorderTop(BorderStyle.MEDIUM);
		
		CellStyle thebodyStyle = workbook.createCellStyle();
		thebodyStyle.setBorderBottom(BorderStyle.THIN);
		thebodyStyle.setBorderLeft(BorderStyle.THIN);
		thebodyStyle.setBorderRight(BorderStyle.THIN);
		thebodyStyle.setBorderTop(BorderStyle.THIN);
				
		//Creamos las filas y campos de datos del cliente
		Row row = sheet.createRow(0);
		Cell cell = row.createCell(0);
		cell.setCellValue(mensaje.getMessage("text.factura.ver.datos.cliente"));
		cell.setCellStyle(theheaderStyle);
		
		row = sheet.createRow(1);
		cell = row.createCell(0);
		cell.setCellValue(factura.getCliente().getNombre() +" "+ factura.getCliente().getApellido());
		
		row = sheet.createRow(2);
		cell = row.createCell(0);
		cell.setCellValue(factura.getCliente().getEmail());
		
		//Creamos las filas y campos de datos de la factura
		//otra forma de crear las lineas y celdas encadenando metodos
		sheet.createRow(4).createCell(0).setCellValue(mensaje.getMessage("text.factura.ver.datos.factura"));
		sheet.createRow(5).createCell(0).setCellValue(mensaje.getMessage("text.cliente.factura.folio") +": " + factura.getId());
		sheet.createRow(6).createCell(0).setCellValue(mensaje.getMessage("text.cliente.factura.descripcion") +": " + factura.getDescripcion());
		sheet.createRow(7).createCell(0).setCellValue(mensaje.getMessage("text.cliente.factura.fecha") +": " + factura.getCreateAt());
		
		
		//creamos el header de la tabla de items
		Row fila= sheet.createRow(sheet.getLastRowNum() + 2);
		fila.createCell(0).setCellValue(mensaje.getMessage("text.factura.form.item.nombre"));
		fila.createCell(1).setCellValue(mensaje.getMessage("text.factura.form.item.precio"));
		fila.createCell(2).setCellValue(mensaje.getMessage("text.factura.form.item.cantidad"));
		fila.createCell(3).setCellValue(mensaje.getMessage("text.factura.form.item.total"));
		
		fila.getCell(0).setCellStyle(theheaderStyle);
		fila.getCell(1).setCellStyle(theheaderStyle);
		fila.getCell(2).setCellStyle(theheaderStyle);
		fila.getCell(3).setCellStyle(theheaderStyle);
		
		
		for(ItemFactura item: factura.getItems()) {
			
			fila= sheet.createRow(sheet.getLastRowNum() + 1);
			fila.createCell(0).setCellValue(item.getProducto().getNombre());
			fila.getCell(0).setCellStyle(thebodyStyle);
			
			fila.createCell(1).setCellValue(item.getProducto().getPrecio());
			fila.getCell(1).setCellStyle(thebodyStyle);
			
			fila.createCell(2).setCellValue(item.getCantidad());
			fila.getCell(2).setCellStyle(thebodyStyle);
			
			fila.createCell(3).setCellValue(item.calcularImporte());
			fila.getCell(3).setCellStyle(thebodyStyle);
		}
		
		fila= sheet.createRow(sheet.getLastRowNum() + 1);
		fila.createCell(2).setCellValue(mensaje.getMessage("text.factura.form.total"));
		fila.getCell(2).setCellStyle(thebodyStyle);
		fila.createCell(3).setCellValue(factura.getTotal());
		fila.getCell(3).setCellStyle(thebodyStyle);
		
	}

}
