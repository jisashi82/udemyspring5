package io.codezone.springboot.form.app.editors;

import java.beans.PropertyEditorSupport;

public class NombreMasyusculasEditor extends PropertyEditorSupport {

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		setValue(text.toUpperCase().trim());
	}
	
}
