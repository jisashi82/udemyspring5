package io.codezone.springboot.form.app.controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import io.codezone.springboot.form.app.editors.NombreMasyusculasEditor;
import io.codezone.springboot.form.app.editors.PaisPropertyEditor;
import io.codezone.springboot.form.app.editors.RolesPropertyEditor;
import io.codezone.springboot.form.app.models.Pais;
import io.codezone.springboot.form.app.models.Role;
import io.codezone.springboot.form.app.models.Usuario;
import io.codezone.springboot.form.app.services.IPaisService;
import io.codezone.springboot.form.app.services.IRoleService;
import io.codezone.springboot.form.app.validation.UsuarioValidador;

/*Se utiliza @Controller para definir un Stereotype de Spring como controlador de las rutas.
 * Se usa @SessionAttributes cuando utilizamos atributos de la clase que no estan mapeados en el formulario 
 * y pueda persistir el valor que se pasa entre cada Request, es necesario SessionStatus para completar*/
@Controller
@SessionAttributes("usuario")
public class FormController {

	@Autowired
	private IRoleService roleService;
	
	@Autowired
	private IPaisService paisService;
	
	@Autowired
	private UsuarioValidador validador;
	
	@Autowired
	private PaisPropertyEditor paisEditor;
	
	@Autowired
	private RolesPropertyEditor rolesEditor;
	
	/*usando InitBinder para desacoplar el validador del metodo @PostMapping*/
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(validador);
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat , true));
		
		binder.registerCustomEditor(String.class, "nombre",new NombreMasyusculasEditor());
		binder.registerCustomEditor(String.class, "apellido",new NombreMasyusculasEditor());
		binder.registerCustomEditor(Pais.class, "pais", paisEditor);
		binder.registerCustomEditor(Role.class, "roles", rolesEditor);
	}

	@GetMapping("/form")
	public String form(Model model) {
		Usuario usuario = new Usuario();
		usuario.setIdentificador("12.456.789-A");
		usuario.setNombre("Jisashi");
		usuario.setApellido("Nakamura");
		usuario.setHabilitar(true);
		usuario.setPais(new Pais(2, "MX", "Mexico"));
		usuario.setRoles(Arrays.asList(new Role(2,"Usuario", "ROLE_USER"),new Role(1,"Administrador", "ROLE_ADMIN")));
		usuario.setValorsecreto("Algun valor secreto $%&/(");
		model.addAttribute("titulo", "Captura del Formulario");
		model.addAttribute("usuario", usuario);
		return "form";
		
	}
		
	@PostMapping("/form")
	public String procesar(@Valid Usuario usuario, BindingResult result, Model model, SessionStatus status) {
		
		//validador.validate(usuario, result); se puede desacoplar en el InitBinder
		
		//model.addAttribute("titulo", "Resultado del Formulario");
		
		if (result.hasErrors()) {
			/*Map<String, String> errores = new HashMap<>();
			 *result.getFieldErrors().forEach(err -> errores.put(err.getField(),
			 *"el campo ".concat(err.getField()).concat(" ").concat(err.getDefaultMessage()
			 *))); model.addAttribute("error", errores);*/
			model.addAttribute("titulo", "Resultado del Formulario");
			return "/form";
		}
		
		
		//model.addAttribute("usuario", usuario);
		
		/*En conjunto con @SessionAtributes se utiliza un SessionStatus para definir el status Complete()*/
		//status.setComplete(); 
		
		return "redirect:/ver";
	}
	
	@GetMapping("/ver")
	public String ver(@SessionAttribute(name="usuario", required = false) Usuario usuario,Model model, SessionStatus status) {
		
		if(usuario == null) {
			return "redirect:/form";
		}
		
		model.addAttribute("titulo", "Resultado del Formulario");
		
		status.setComplete();
		return "resultado";
	}

	@ModelAttribute("listapaises")
	public List<Pais> listapaises() {
		return paisService.listar();
	}
	
	@ModelAttribute("paises")
	public List<String> paises() {
		return Arrays.asList("España", "Mexico", "Chile", "Venezuela", "Colombia", "Argentina");
	}
	
	@ModelAttribute("paisesMap")
	public Map<String, String> paisesMap() {
		Map<String, String> paises= new HashMap<String, String>();
		paises.put("ES", "España");
		paises.put("MX", "Mexico");
		paises.put("CL", "Chile");
		paises.put("VE", "Venezuela");
		paises.put("CO", "Colombia");
		paises.put("AR", "Argentina");
		
		return paises;
	}
	
	@ModelAttribute("listarolesString")
	public List<String> listarolesString() {
		List<String> roles= new ArrayList<String>();
		roles.add("ROLE_ADMIN");
		roles.add("ROLE_USER");
		roles.add("ROLE_GUEST");
		
		return roles;
	}
	
	@ModelAttribute("listarolesMap")
	public Map<String, String> listarolesMap() {
		Map<String, String> roles= new HashMap<String, String>();
		roles.put("ROLE_ADMIN", "Administrador");
		roles.put("ROLE_USER", "Usuario");
		roles.put("ROLE_GUEST", "Invitado");
		
		return roles;
	}
	
	@ModelAttribute("listaRoles")
	public List<Role> listaRoles() {
		return roleService.listar();
	}
	
	@ModelAttribute("listagenero")
	public List<String> listaGenero() {
		return Arrays.asList("Hombre", "Mujer");
	}
}
