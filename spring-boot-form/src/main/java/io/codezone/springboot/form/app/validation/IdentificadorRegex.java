package io.codezone.springboot.form.app.validation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

/*utilizando anotacion nativa de la API de validacion de java Java.beans.validator; Solo valida un cierto tipo de campo*/

@Constraint(validatedBy = IdentificadorRegexValidador.class)
@Retention(RUNTIME)
@Target({ FIELD, METHOD })
public @interface IdentificadorRegex {
	
	String message() default "Identificador Invalido desde anotaciones";

	Class<?>[] groups() default { };

	Class<? extends Payload>[] payload() default { };

}
