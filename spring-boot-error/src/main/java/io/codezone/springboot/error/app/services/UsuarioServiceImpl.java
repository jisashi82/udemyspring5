package io.codezone.springboot.error.app.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import io.codezone.springboot.error.app.errors.UsuarioNoEncontradoException;
import io.codezone.springboot.error.app.models.domain.Usuario;

@Service
public class UsuarioServiceImpl implements IUsuarioService {

	private List<Usuario> lista;

	public UsuarioServiceImpl() {
		this.lista = new ArrayList<>();
		this.lista.add(new Usuario(1, "Abel", "Ramos"));
		this.lista.add(new Usuario(2, "Ana", "Cruz"));
		this.lista.add(new Usuario(3, "Jisashi", "Ramos"));
		this.lista.add(new Usuario(4, "Adolfo", "Cruz"));
		this.lista.add(new Usuario(5, "Alondra", "Reyes"));
		this.lista.add(new Usuario(6, "Mavi", "Cruz"));
	}

	@Override
	public List<Usuario> listar() {
		return this.lista;
	}

	@Override
	public Usuario obtenerPorId(Integer id) {
		Usuario resultado = null;
//		try {
//			resultado = this.lista.stream().filter(u -> u.getId().equals(id)).findFirst().orElseGet(null);
//		} catch (Exception e) {
//			throw new UsuarioNoEncontradoException(id.toString());
//		}
		resultado = this.lista.stream().filter(u -> u.getId().equals(id)).findFirst()
				.orElseThrow(() -> new UsuarioNoEncontradoException(id.toString()));
		return resultado;
	}

}
