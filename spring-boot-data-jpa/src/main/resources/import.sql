INSERT INTO clientes(nombre,apellido,email,create_at,foto) VALUES('Abel', 'Ramos', 'abel@gmail.com', '2021-06-15','');
INSERT INTO clientes(nombre,apellido,email,create_at,foto) VALUES('John', 'Doe', 'jdoe@gmail.com', '2021-06-15','');
INSERT INTO clientes(nombre,apellido,email,create_at,foto) VALUES('Gabriel', 'Ramos', 'gabriel@gmail.com', '2021-06-16','');
INSERT INTO clientes(nombre,apellido,email,create_at,foto) VALUES('Miguel', 'Doe', 'jdoe@gmail.com', '2021-06-17','');
INSERT INTO clientes(nombre,apellido,email,create_at,foto) VALUES('Romeo', 'Ramos', 'abel@gmail.com', '2021-06-18','');
INSERT INTO clientes(nombre,apellido,email,create_at,foto) VALUES('Juan', 'Doe', 'jdoe@gmail.com', '2021-06-15','');
INSERT INTO clientes(nombre,apellido,email,create_at,foto) VALUES('Carlos', 'Ramos', 'abel@gmail.com', '2021-06-19','');
INSERT INTO clientes(nombre,apellido,email,create_at,foto) VALUES('Javier', 'Doe', 'jdoe@gmail.com', '2021-06-11','');
INSERT INTO clientes(nombre,apellido,email,create_at,foto) VALUES('Cristina', 'Ramos', 'abel@gmail.com', '2021-06-12','');
INSERT INTO clientes(nombre,apellido,email,create_at,foto) VALUES('Federic', 'Doe', 'jdoe@gmail.com', '2021-06-15','');
INSERT INTO clientes(nombre,apellido,email,create_at,foto) VALUES('Luis', 'Ramos', 'abel@gmail.com', '2021-06-13','');
INSERT INTO clientes(nombre,apellido,email,create_at,foto) VALUES('Fernando', 'Doe', 'jdoe@gmail.com', '2021-06-14','');
INSERT INTO clientes(nombre,apellido,email,create_at,foto) VALUES('Ramona', 'Ramos', 'abel@gmail.com', '2021-06-15','');
INSERT INTO clientes(nombre,apellido,email,create_at,foto) VALUES('Kari', 'Doe', 'jdoe@gmail.com', '2021-06-15','');
INSERT INTO clientes(nombre,apellido,email,create_at,foto) VALUES('Alberto', 'Ramos', 'abel@gmail.com', '2021-06-15','');
INSERT INTO clientes(nombre,apellido,email,create_at,foto) VALUES('John', 'Doe', 'jdoe@gmail.com', '2021-06-15','');
INSERT INTO clientes(nombre,apellido,email,create_at,foto) VALUES('Richard', 'Ramos', 'abel@gmail.com', '2021-06-15','');
INSERT INTO clientes(nombre,apellido,email,create_at,foto) VALUES('John', 'Doe', 'jdoe@gmail.com', '2021-06-15','');
INSERT INTO clientes(nombre,apellido,email,create_at,foto) VALUES('Abel', 'Ramos', 'abel@gmail.com', '2021-06-15','');
INSERT INTO clientes(nombre,apellido,email,create_at,foto) VALUES('John', 'Doe', 'jdoe@gmail.com', '2021-06-15','');
INSERT INTO clientes(nombre,apellido,email,create_at,foto) VALUES('Abel', 'Ramos', 'abel@gmail.com', '2021-06-15','');
INSERT INTO clientes(nombre,apellido,email,create_at,foto) VALUES('John', 'Doe', 'jdoe@gmail.com', '2021-06-15','');
INSERT INTO clientes(nombre,apellido,email,create_at,foto) VALUES('Abel', 'Ramos', 'abel@gmail.com', '2021-05-15','');
INSERT INTO clientes(nombre,apellido,email,create_at,foto) VALUES('John', 'Doe', 'jdoe@gmail.com', '2021-05-28','');
INSERT INTO clientes(nombre,apellido,email,create_at,foto) VALUES('Abel', 'Ramos', 'abel@gmail.com', '2021-05-21','');
INSERT INTO clientes(nombre,apellido,email,create_at,foto) VALUES('John', 'Doe', 'jdoe@gmail.com', '2021-05-16','');

/*populate table Productos*/
INSERT INTO productos(nombre, precio, create_at) VALUES('Panasonic Pantalla LCD 32in',4500,NOW());
INSERT INTO productos(nombre, precio, create_at) VALUES('Sony Camara Digital DSC-W320B',7950,NOW());
INSERT INTO productos(nombre, precio, create_at) VALUES('Apple iPod shuffle',12500,NOW());
INSERT INTO productos(nombre, precio, create_at) VALUES('Sony NoteBook Z110',37990,NOW());
INSERT INTO productos(nombre, precio, create_at) VALUES('Hewlett Packard Multifuncional F2280',69567,NOW());
INSERT INTO productos(nombre, precio, create_at) VALUES('Bianchi Bicicleta Aro 26',13500,NOW());
INSERT INTO productos(nombre, precio, create_at) VALUES('Mica Comoda 5 Cajones',19700,NOW());
INSERT INTO productos(nombre, precio, create_at) VALUES('Huawei NoteBook M13',22500,NOW());
INSERT INTO productos(nombre, precio, create_at) VALUES('Acer NoteBook A1',15600,NOW());
INSERT INTO productos(nombre, precio, create_at) VALUES('Canon Camara C65',22000,NOW());

/*creamos algunas facturas*/
INSERT INTO facturas(descripcion, observacion, cliente_id, create_at) VALUES('Factura de equipos de Oficina', null, 1, NOW());
INSERT INTO facturas_items(cantidad, factura_id, producto_id) VALUES(1,1,1);
INSERT INTO facturas_items(cantidad, factura_id, producto_id) VALUES(2,1,4);
INSERT INTO facturas_items(cantidad, factura_id, producto_id) VALUES(1,1,5);
INSERT INTO facturas_items(cantidad, factura_id, producto_id) VALUES(1,1,7);

INSERT INTO facturas(descripcion, observacion, cliente_id, create_at) VALUES('Factura de Bicicleta', 'Alguna nota importante', 1, NOW());
INSERT INTO facturas_items(cantidad, factura_id, producto_id) VALUES(3,2,6);

/*creamos algunos usuarios con sus roles para JPA*/
INSERT INTO users(username, password, enabled) VALUES ('abel','{bcrypt}$2a$10$Cv3ZkRAli1TDU/bjWejxDuu6XthAgXEfJ77aT3GHb/BDxNLeIko4q',true);
INSERT INTO users(username, password, enabled) VALUES ('admin','{bcrypt}$2a$10$R.AOiphoP99rN1V2runwwukrdl6qrs1uRLaKmZzhwfIFY2uipee4W',true);

INSERT INTO authorities(user_id, authority) VALUES (1,'ROLE_USER');
INSERT INTO authorities(user_id, authority) VALUES (2,'ROLE_USER');
INSERT INTO authorities(user_id, authority) VALUES (2,'ROLE_ADMIN');