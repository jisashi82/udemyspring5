package io.codezone.springboot.app.controllers;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import io.codezone.springboot.app.models.entity.Cliente;
import io.codezone.springboot.app.models.service.IClienteService;
import io.codezone.springboot.app.models.service.IUploadFileService;
import io.codezone.springboot.app.util.paginator.PageRender;
import io.codezone.springboot.app.view.xml.ClienteList;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;

@Controller
@SessionAttributes("cliente")
public class ClienteController {

	@Autowired
	private IClienteService clienteService;

	@Autowired
	private IUploadFileService uploadFileService;

	@Autowired
	private MessageSource messageSource;
	
	private final Log logger = LogFactory.getLog(this.getClass());

	@GetMapping(value = "listar2")
	public String listar(Model model) {
		model.addAttribute("titulo", "listado de clientes");
		model.addAttribute("clientes", clienteService.findAll());
		return "listar";
	}

	@GetMapping(value = "/listar-rest")
	@ResponseBody
	public ClienteList listarREST() {
		/*
		 * Esta forma devuelve el listado de clientes en formato JSON + XML al usar 
		 * la URL http://localhost:8080/listar-rest?format=xml
		 */
		return new ClienteList(clienteService.findAll());
	}
	
	@GetMapping(value = { "/listar", "/" })
	public String listarPage(@RequestParam(name = "page", defaultValue = "0") int page,
			Model model, Authentication authentication, HttpServletRequest request, Locale locale) {

		/* 1a forma.- Obteniendo el usuario autenticado y mostrandolo en la consola, mediante
		injeccion de dependencias como argumento/parametro Authentication */
		if (authentication != null) {
			logger.info("Authentication: Hola Usuario autenticado: "
			.concat(authentication.getName())
			.concat(" tu roles son: ").concat(authentication.getAuthorities().toString()));
		}

		/* 2a Forma estatica de obtener el usuario autenticado y role con SecurityContextHolder */
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			logger.info("Usuario autenticado, tu username es: ".concat(auth.getName()));

			if (auth.isAuthenticated() && hasRole("ROLE_ADMIN")) {
				logger.info("SecurityContextHolder: Hola ".concat(auth.getName()).concat(" tienes acceso!, tu role es: ")
						.concat(auth.getAuthorities().toString()));
			} else {
				logger.info("SecurityContextHolder: Hola ".concat(auth.getName()).concat(" NO tienes acceso!"));
			}
		}

		/* 3a Forma de obtener el usuario autenticado con SecurityContextHolderAwareRequestWrapper, 
		es necesario usar el parametro HttpServletRequest */
		SecurityContextHolderAwareRequestWrapper requestWrapper = new SecurityContextHolderAwareRequestWrapper(request,	"ROLE_");
		if (auth != null) {
			if (requestWrapper.isUserInRole("ROLE_ADMIN")) {
				logger.info("SecurityContextHolderAwareRequestWrapper: Hola"
						.concat(requestWrapper.getRemoteUser())
						.concat(" tienes acceso!, tu role es:")
						.concat(auth.getAuthorities().toString()));
			} else {
				logger.info("Hola ".concat(auth.getName()).concat(" NO tienes acceso!"));
			}
		}

		/* 4a Forma de obtener el usuario y sus roles de manera nativa utilizando el request */
		if (request.isUserInRole("ROLE_ADMIN")) {
			logger.info("Forma HttpServletRequest:Hola "
					.concat(request.getRemoteUser())
					.concat(" tienes acceso!, tu role es:")
					.concat(((Authentication) request.getUserPrincipal()).getAuthorities().toString()));
		}

		Pageable pageRequest = PageRequest.of(page, 10);
		Page<Cliente> clientes = clienteService.findAll(pageRequest);
		PageRender<Cliente> pageRender = new PageRender<>("/listar", clientes);
		model.addAttribute("titulo", messageSource.getMessage("text.cliente.listar.titulo", null, locale));
		model.addAttribute("clientes", clientes);
		model.addAttribute("page", pageRender);
		return "listar";
	}

	@Secured("ROLE_ADMIN")
	@GetMapping("/form")
	public String crear(Map<String, Object> model) {
		model.put("titulo", "Formulario de Cliente");
		model.put("cliente", new Cliente());

		return "form";
	}

	@Secured("ROLE_USER")
	@GetMapping(value = "/uploads/{filename:.+}")
	public ResponseEntity<Resource> verfoto(@PathVariable String filename) {
		Resource recurso = null;
		try {
			recurso = uploadFileService.load(filename);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + recurso.getFilename() + "\"")
				.body(recurso);
	}

	@Secured("ROLE_ADMIN")
	@PostMapping(value = "/form")
	public String guardar(@Valid Cliente cliente, BindingResult result, Model model,
			@RequestParam("file") MultipartFile foto, RedirectAttributes flash, SessionStatus status) {

		if (result.hasErrors()) {
			model.addAttribute("titulo", "Formulario de Cliente");
			return "form";
		}

		// valida si la foto no viene vacia
		if (!foto.isEmpty()) {

			if (cliente.getId() != null && cliente.getId() > 0 && cliente.getFoto() != null
					&& cliente.getFoto().length() > 0) {
				uploadFileService.delete(cliente.getFoto());
			}

			String uniqueFilename = null;
			try {
				uniqueFilename = uploadFileService.copy(foto);
			} catch (IOException e) {
				e.printStackTrace();
			}

			cliente.setFoto(uniqueFilename);
			flash.addFlashAttribute("message", "Has subido correctamente '" + uniqueFilename + "'");
			flash.addFlashAttribute("alertClass", "alert-info");

		}

		String mensajeFlash = (cliente.getId() != null) ? "Cliente editado con éxito!" : "Cliente guardado con éxito!";
		clienteService.save(cliente);
		flash.addFlashAttribute("message", mensajeFlash);
		flash.addFlashAttribute("alertClass", "alert-success");
		status.setComplete();
		return "redirect:/listar";
	}

	@Secured("ROLE_ADMIN")
	@GetMapping(value = "/form/{id}")
	public String editar(@PathVariable(value = "id") Long id, Map<String, Object> model, RedirectAttributes flash) {
		Cliente cliente = null;
		if (id > 0) {
			cliente = clienteService.findOne(id);
			if (cliente == null) {
				flash.addFlashAttribute("message", "El ID del cliente no existe en la BBDD");
				flash.addFlashAttribute("alertClass", "alert-warning");
				return "redirect:/listar";
			}
		} else {
			flash.addFlashAttribute("message", "El ID del cliente no puede ser cero");
			flash.addFlashAttribute("alertClass", "alert-warning");
			return "redirect:/listar";
		}

		model.put("cliente", cliente);
		model.put("titulo", "Editar Cliente");

		return "form";
	}

	@Secured("ROLE_ADMIN")
	@GetMapping(value = "/eliminar/{id}")
	public String eliminar(@PathVariable Long id, RedirectAttributes flash) {
		if (id > 0) {
			Cliente cliente = clienteService.findOne(id);

			if (uploadFileService.delete(cliente.getFoto())) {
				flash.addFlashAttribute("message", "Foto " + cliente.getFoto() + " eliminada con exito!");
				flash.addFlashAttribute("alertClass", "alert-danger");
			}

			clienteService.delete(id);
			flash.addFlashAttribute("message", "Cliente Eliminado con Exito");
			flash.addFlashAttribute("alertClass", "alert-danger");

		}
		return "redirect:/listar";
	}

	@Secured({"ROLE_ADMIN", "ROLE_USER"})
	@GetMapping(value = "/ver/{id}")
	public String ver(@PathVariable(value = "id") Long id, Model model, RedirectAttributes flash, Locale locale) {
		Cliente cliente = clienteService.fetchByIdWithFacturas(id);
		// Cliente cliente = clienteService.findOne(id);
		if (cliente == null) {
			flash.addFlashAttribute("message", messageSource.getMessage("text.cliente.noEncontrado", null, locale));
			flash.addFlashAttribute("alertClass", "alert-warning");
			return "redirect:/listar";
		}

		model.addAttribute("cliente", cliente);
		model.addAttribute("titulo", messageSource.getMessage("text.cliente.ver.titulo", null, locale) + cliente.getNombre());
		return "ver";
	}

	// Metodo para obtener el role del usuario
	private boolean hasRole(String role) {
		SecurityContext context = SecurityContextHolder.getContext();
		if (context == null) {
			return false;
		}
		Authentication auth = context.getAuthentication();
		if (auth == null) {
			return false;
		}
		Collection<? extends GrantedAuthority> authorities = auth.getAuthorities();

		/*
		 * Iteramos la lista de roles del usuario y verificamos si coincide con el rol
		 * que se pasa por parametro
		 * for (GrantedAuthority authority : authorities) {
		 * if (authority.getAuthority().equals(role)) {
		 * logger.info("Hola usuario ".concat(auth.getName()).concat(" tu role es: ").
		 * concat(authority.getAuthority()));
		 * return true;
		 * }
		 */

		return authorities.contains(new SimpleGrantedAuthority(role));
	}

}
