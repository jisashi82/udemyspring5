package io.codezone.springboot.web.app.controllers;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import io.codezone.springboot.web.app.models.Usuario;

@Controller
public class IndexController {
	
	@Value("${texto.indecontroller.index.titulo}") private String textoIndex;
	@Value("${texto.indecontroller.perfil.titulo}") private String textoPerfil;
	@Value("${texto.indecontroller.listar.titulo}") private String textoListar;

	@GetMapping({ "/index", "/", "/home" })
	public String index(Model model) {
		model.addAttribute("titulo", textoIndex);
		return "index";
	}

	@GetMapping("/perfil")
	private String perfil(Model model) {
		Usuario usuario= new Usuario("Abel", "Ramos","AbelR@estriclan.com");
		model.addAttribute("usuario", usuario);
		model.addAttribute("titulo", textoPerfil.concat(usuario.getNombre()));
		return "perfil";
	}
	
	@GetMapping("/listar")
	private String listar(Model model) {
	
		model.addAttribute("titulo", textoListar);
		return "listar";
	}
	
	//ModelAttribute funciona para poblar datos que son comunes en todo el controlador, ej. combobox ciudades
	@ModelAttribute("usuarios")
	public List<Usuario> poblarUsuarios(){
		List<Usuario> usuarios= Arrays.asList(
				new Usuario("Maria", "Lopez",""), 
				new Usuario("Pedro", "Ramirez", "Pramirez@gmail.com"),
				new Usuario("Jon", "Bright","jbrigh2cris.com")
				);
		
		return usuarios;
	}

}
