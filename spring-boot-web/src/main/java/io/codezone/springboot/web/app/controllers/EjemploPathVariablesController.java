package io.codezone.springboot.web.app.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/variables")
public class EjemploPathVariablesController {

	@GetMapping("/string/{texto}")
	public String variable(@PathVariable String texto, Model model) {
		model.addAttribute("titulo", "Recibir Parametros de la Ruta-URL(@PathVariable)");
		model.addAttribute("resultado", "El resultado del valor del Path es: "+texto);
		return "variables/ver";
		
	}
	
	@GetMapping("/string/{texto}/{numero}")
	public String variable(@PathVariable String texto,@PathVariable Integer numero ,Model model) {
		model.addAttribute("titulo", "Recibir Parametros de la Ruta-URL(@PathVariable)");
		model.addAttribute("resultado", "El resultado del valor del Path es: "+texto +" y el nuemro enviado es: " +numero);
		return "variables/ver";
		
	}
}
