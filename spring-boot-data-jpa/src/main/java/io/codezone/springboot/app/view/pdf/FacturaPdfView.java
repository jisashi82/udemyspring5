package io.codezone.springboot.app.view.pdf;

import java.awt.Color;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import com.lowagie.text.Document;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import io.codezone.springboot.app.models.entity.Factura;
import io.codezone.springboot.app.models.entity.ItemFactura;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Component("factura/ver")
public class FacturaPdfView extends AbstractPdfView {
	
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private LocaleResolver localeResolver;

	@Override
	protected void buildPdfDocument(Map<String, Object> model, Document document, PdfWriter writer,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		//obtenemos la factura mediante el modelo, como es unb objecto de tipo factura se debe castear
		Factura factura = (Factura) model.get("factura");
		//Creamos una celda para customizar el encabezado
		PdfPCell cell = null;
		
		//obtenemos el locale para traducir los textos
		Locale locale = localeResolver.resolveLocale(request);
		
		//2a forma para traducir los textos reutilizando el metodo getMessage de la clase AbstractPdfView que hereda o implementae ApplicationObjectSupport
		MessageSourceAccessor mensaje = getMessageSourceAccessor();
		
		//Creamos una tabla para los datos del cliente y customizamos el encabezado
		PdfPTable tabla = new PdfPTable(1);
		tabla.setSpacingAfter(20);
		
		
		cell = new PdfPCell(new Phrase(messageSource.getMessage("text.factura.ver.datos.cliente", null, locale), FontFactory.getFont(FontFactory.HELVETICA_BOLD, 14)));
		cell.setBackgroundColor(new Color(184, 218, 255));
		cell.setPadding(8f);
		tabla.addCell(cell);
		tabla.addCell(factura.getCliente().getNombre()+ " " + factura.getCliente().getApellido());
		tabla.addCell(factura.getCliente().getEmail());
		
		
		//Creamos una tabla para los datos de la factura y customizamos el encabezado
		PdfPTable tabla2 = new PdfPTable(1);
		tabla2.setSpacingAfter(20);		
		cell = new PdfPCell(new Phrase(messageSource.getMessage("text.factura.ver.datos.factura",null,locale), FontFactory.getFont(FontFactory.HELVETICA_BOLD, 14)));
		cell.setBackgroundColor(new Color(195, 230, 203));
		cell.setPadding(8f);
		tabla2.addCell(cell);
		tabla2.addCell(mensaje.getMessage("text.cliente.factura.folio") + ": " + factura.getId());
		tabla2.addCell(mensaje.getMessage("text.cliente.factura.descripcion") + ": " + factura.getDescripcion());
		tabla2.addCell(mensaje.getMessage("text.cliente.factura.fecha") + ": " + factura.getCreateAt());
		
		
		//Creamos una tabla para los datos delos items de productos
		PdfPTable tabla3 = new PdfPTable(4);
		//asignamos el ancho de las columnas
		tabla3.setWidths(new float[] { 2.5f, 1f, 1f, 1f });
		tabla3.addCell(mensaje.getMessage("text.factura.form.item.nombre"));
		tabla3.addCell(mensaje.getMessage("text.factura.form.item.cantidad"));
		tabla3.addCell(mensaje.getMessage("text.factura.form.item.precio"));
		tabla3.addCell(mensaje.getMessage("text.factura.form.item.total"));
		
		//con un ciclo recorremos los items y los agregamos a la tabla
		for(ItemFactura item : factura.getItems()) {
			tabla3.addCell(item.getProducto().getNombre());
			
			cell=new PdfPCell(new Phrase(item.getCantidad().toString()));
			cell.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
			tabla3.addCell(cell);
			tabla3.addCell(String.valueOf(item.getProducto().getPrecio().toString()));
			tabla3.addCell(String.valueOf(item.calcularImporte().toString()));
		}
		
		cell = new PdfPCell(new Phrase(mensaje.getMessage("text.factura.form.total") + " : ", FontFactory.getFont(FontFactory.HELVETICA_BOLD, 14)));
		cell.setColspan(3);
		cell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
		tabla3.addCell(cell);
		tabla3.addCell(factura.getTotal().toString());
		
		
		//agregamos las tablas a la pagina o documento
		document.add(tabla);
		document.add(tabla2);
		document.add(tabla3);
	}

}
