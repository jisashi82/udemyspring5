package io.codezone.springboot.app;

/* Se utiliza cuando la authenticacion es por JDBC 
import javax.sql.DataSource;*/

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
//import org.springframework.security.web.servlet.util.matcher.MvcRequestMatcher;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import io.codezone.springboot.app.auth.handler.LoginSuccessHandler;
import io.codezone.springboot.app.auth.handler.LogoutSuccessHandler;
import io.codezone.springboot.app.models.service.JpaUserDetailsService;

import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;

//@EnableWebSecurity
@Configuration
@EnableMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfig {

        @Autowired
        private LoginSuccessHandler successHandler;

        @Autowired
        private LogoutSuccessHandler logoutSuccessHandler;

       /* Se utiliza cuando la authenticacion es por JDBC  
        @Autowired
        private DataSource dataSource; */

        /* Se utiliza cuando la authenticacion es por JPA,JDBC e InmemoryAthentication ya que sirve para cifrar las contraseñas */
        @Autowired
        private PasswordEncoder passwordEncoder;

        /* Se inyecta clase para uso de Authenticacion JPA */
        @Autowired
        private JpaUserDetailsService jpaUserDetailsService;

        //Authentication in memory
       /*  @Bean
        // @ConditionalOnMissingBean(UserDetailsService.class)
        public UserDetailsService userDetailsService() throws Exception {
                PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
                InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
                manager.createUser(User.withUsername("admin").password(encoder.encode("admin")).roles("ADMIN", "USER")
                                .build());
                manager.createUser(User.withUsername("abel").password(encoder.encode("12345")).roles("USER").build());

                return manager;
        }    */
        
        /*Authentication in database JDBC */
       /*  @Bean
        public UserDetailsManager userDetailsManager(AuthenticationManagerBuilder auth) throws Exception {                

                return auth
                .jdbcAuthentication()
                .dataSource(dataSource)
                .passwordEncoder(passwordEncoder)
                .usersByUsernameQuery("select username, password, enabled from users where username=?")
                .authoritiesByUsernameQuery("select u.username,a.authority from authorities a inner join users u on (a.user_id = u.id) where u.username=?")
                .getUserDetailsService();
                
                
                /* JdbcUserDetailsManager users= new JdbcUserDetailsManager(dataSource);
                users.setUsersByUsernameQuery("select username, password, enabled from users where username=?");
                users.setAuthoritiesByUsernameQuery("select u.username,a.authority from authorities a inner join users u on (a.user_id = u.id) where u.username=?");
                return configurer.getUserDetailsService();*/
        //}

        /* Autenticacion con JPA.- se crea la clase JpaUserDetailsService que hereda de UserDetailsService, se inyecta por Autowired
         y se añade a la configuración */
        @Bean
        public UserDetailsService userDetailsService(AuthenticationManagerBuilder auth) throws Exception {
                return auth.userDetailsService(jpaUserDetailsService).passwordEncoder(passwordEncoder).getUserDetailsService();
        }        

        /*Este Bloque es para configurar la seguridad del acceso a las URL, la forma de uso cambio de la version 5.0 a la 6.0
         * ya que utiliza las funciones lambda */
        @Bean
        public SecurityFilterChain configure(HttpSecurity http) throws Exception {
                //MvcRequestMatcher m = new MvcRequestMatcher(null, "/ver/**");
                http
                        .csrf(Customizer.withDefaults())
                        .authorizeHttpRequests(authorize -> authorize
                                        /* .requestMatchers(m).hasAnyRole("USER")
                                        .requestMatchers((req) -> req.getRequestURI().contains("/editar")).hasRole("ADMIN")
                                        .requestMatchers((req) -> req.getRequestURI().contains("/eliminar"))
                                        .hasRole("ADMIN")
                                        .requestMatchers((req) -> req.getRequestURI().contains("/factura/"))
                                        .hasRole("ADMIN")
                                        .requestMatchers((req) -> req.getRequestURI().contains("/form"))
                                        .hasRole("ADMIN") */
                                        .requestMatchers(new AntPathRequestMatcher("/css/**"),
                                                        new AntPathRequestMatcher("/js/**"),
                                                        new AntPathRequestMatcher("/uploads"),
                                                        new AntPathRequestMatcher("/listar**"),
                                                        new AntPathRequestMatcher("/"), 
                                                        new AntPathRequestMatcher("/locale"),
														new AntPathRequestMatcher("/api/clientes/**"))
                                        .permitAll()
                                        .anyRequest().authenticated())
                        .httpBasic(Customizer.withDefaults())
                        .formLogin(formLogin -> formLogin
                                        .usernameParameter("username")
                                        .passwordParameter("password")
                                        .loginPage("/login")
                                        .defaultSuccessUrl("/listar", true)
                                        .successHandler(successHandler)
                                        .permitAll())
                        .logout(logout -> logout
                                        .logoutUrl("/logout")
                                        .logoutSuccessUrl("/login?logout")
                                        .logoutSuccessHandler(logoutSuccessHandler)
                                        .permitAll())
                        .exceptionHandling(exceptionHandling -> exceptionHandling
                                        .accessDeniedPage("/error_403"));

                return http.build();
        }

}
