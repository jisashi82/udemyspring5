package io.codezone.springboot.form.app.services;

import java.util.List;

import io.codezone.springboot.form.app.models.Role;

public interface IRoleService {

	public List<Role> listar();
	public Role obtenerPorId(Integer id);
}
