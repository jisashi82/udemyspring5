package io.codezone.springboot.app.view.json;

import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import io.codezone.springboot.app.models.entity.Cliente;

/*
 * Esta Clase genera una vista Json, como json ya es nativo de Spring no es necesario configuraciones en el archivo properties
 *  ni Wrappers como el caso de XML, Solo se hereda de MappingJackson2JsonView
 */

@Component("listar.json")
public class ClienteListJsonView extends MappingJackson2JsonView {
	
	@SuppressWarnings("unchecked")
	@Override
	protected Object filterModel(Map<String, Object> model) {
		
		Page<Cliente> clientes = (Page<Cliente>) model.get("clientes");
		model.remove("titulo");
		model.remove("page");
		model.remove("clientes");
		
		model.put("clientes", clientes.toList());
		
		return super.filterModel(model);
	}

}
