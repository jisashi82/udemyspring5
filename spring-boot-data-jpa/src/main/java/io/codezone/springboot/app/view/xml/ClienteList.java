package io.codezone.springboot.app.view.xml;

import java.util.List;

import io.codezone.springboot.app.models.entity.Cliente;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;



/*
 * Esta es la clase wrapper para los XML
 */

@XmlRootElement(name = "clientes")
public class ClienteList {
	
	@XmlElement(name = "cliente")
	public List<Cliente> clientesList;
	
	public ClienteList() {		
	}
	
	public ClienteList(List<Cliente> clientes) {
		this.clientesList = clientes;
	}
	
	
	public List<Cliente> getClientes() {
		return clientesList;
	}

	public void setClientesList(List<Cliente> clientesList) {
		this.clientesList = clientesList;
	}
		

}
