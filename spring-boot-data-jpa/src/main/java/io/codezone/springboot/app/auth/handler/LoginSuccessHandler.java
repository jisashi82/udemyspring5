package io.codezone.springboot.app.auth.handler;

import java.io.IOException;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.FlashMap;
import org.springframework.web.servlet.support.SessionFlashMapManager;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Component
public class LoginSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, 
            Authentication authentication) throws IOException, ServletException {

        SessionFlashMapManager flashMapMngr = new SessionFlashMapManager();
        FlashMap flashMap = new FlashMap();
        flashMap.put("message", "Hola " + authentication.getName() + ", has iniciado sesión con éxito!");
        flashMap.put("alertClass", "alert-success");
        flashMapMngr.saveOutputFlashMap(flashMap, request, response);

        if (authentication != null) {
            logger.info(authentication.getName() + " ha iniciado sesión con éxito!");
        }

        super.onAuthenticationSuccess(request, response, authentication);
    }
    
}
