package io.codezone.springboot.app.controllers;

import java.security.Principal;

//import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class LoginController {

    //private SecurityContextLogoutHandler logoutHandler = new SecurityContextLogoutHandler();

    @GetMapping("/login")
    public String login(@RequestParam(value = "error", required = false) String error,
            @RequestParam(value = "logout", required = false) String logout,
            Model model, Principal principal, RedirectAttributes flash) {
        model.addAttribute("titulo", "Login Page");

        if (principal != null) {
            flash.addFlashAttribute("alertClass", "alert-info");
            flash.addFlashAttribute("message", "Ya estás logueado como " + principal.getName());
            return "redirect:/listar";
        }

        if (error != null) {
            flash.addFlashAttribute("alertClass", "alert-danger");
            model.addAttribute("message", "Usuario o contraseña incorrectos");
        }
        if (logout != null) {
            flash.addFlashAttribute("alertClass", "alert-success");
            model.addAttribute("message", "Ha cerrado sesión con éxito");
        }

        return "login";
    }

    // @PostMapping("/my-logout")
    // public String logout(Authentication auth, HttpServletRequest request, HttpServletResponse response) {
    //     logoutHandler.setInvalidateHttpSession(true);
    //     this.logoutHandler.logout(request, response, auth);
    //     return "redirect:/login";
    // }

}
