package io.codezone.springboot.form.app.interceptors;

import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

@Component("tiempoTranscurridoInterceptor")
public class TiempoTranscurridoInterceptor implements HandlerInterceptor {

	private static final Logger logger = LoggerFactory.getLogger(TiempoTranscurridoInterceptor.class);

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		// para que el interceptor no se aplique al metodo POST /form del controller
		// solo al GET
		if (request.getMethod().equalsIgnoreCase("post")) {
			return true;
		}

		// como demostracion para debug
		if (handler instanceof HandlerMethod) {
			HandlerMethod metodo = (HandlerMethod) handler;
			logger.info("Es un metodo del controlador: " + metodo.getMethod().getName());
		}

		logger.info("TiempoTranscurridoInterceptor: preHandler() entrando ...");
		logger.info("Interceptando: " + handler);
		long tiempoinicio = System.currentTimeMillis();
		request.setAttribute("tiempoinicio", tiempoinicio);

		Random ramdom = new Random();
		Integer demora = ramdom.nextInt(200);
		Thread.sleep(demora);

		return true;

		/*
		 * el metodo sendRedirect cuando en autenticacion no se cumple el requisito de
		 * estar logueado se redirige a la pagina de login
		 * response.sendRedirect(request.getContextPath().concat("/login")); return
		 * false;
		 */

	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {

		/* para que el interceptor no se aplique al metodo POST /form del controller
		// solo al GET se debe retornar vacio(void) con return;*/
		if (request.getMethod().equalsIgnoreCase("get")) {
			long tiempofin = System.currentTimeMillis();
			long tiempoinicio = (Long) request.getAttribute("tiempoinicio");
			long tiempotranscurrido = tiempofin - tiempoinicio;

			/* validacion estricta con HandlerMethod, tambien puede fucionar solo validando	con ModelAndView*/
			if (handler instanceof HandlerMethod && modelAndView != null) {
				modelAndView.addObject("tiempotranscurrido", tiempotranscurrido);
			}

			logger.info("Tiempo Transcurrido: " + tiempotranscurrido + " milisegundos");
			logger.info("TiempoTranscurridoInterceptor: postHandler() saliendo ...");
		} else {
			return;
		}

	}

}
