package io.codezone.springboot.app.view.xml;

import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.xml.MarshallingView;

import io.codezone.springboot.app.models.entity.Cliente;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Component("listar.xml")
public class ClienteListXmlView extends MarshallingView {
	
	//@Autowired	
	public ClienteListXmlView(Jaxb2Marshaller marshaller) {
		super(marshaller);
	}

	@Override
	protected void renderMergedOutputModel(Map<String, Object> model, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		// Se obtiene la lista de clientes painados
		@SuppressWarnings("unchecked")
		Page<Cliente> clientes = (Page<Cliente>) model.get("clientes");

		// Se elimina del modelo los atributos que no se van a usar
		model.remove("titulo");
		model.remove("page");
		model.remove("clientes");
			
		// Se agrega al modelo con getContent() para transformar de Page<Cliente> a List<Cliente>
		model.put("clientes", new ClienteList(clientes.getContent()));

		super.renderMergedOutputModel(model, request, response);
	}

}
