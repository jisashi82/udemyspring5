package io.codezone.springboot.form.app.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class RequeridoValidador implements ConstraintValidator<Requerido, String> {

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		/*tambien se puede utilizar la negacion del Helper StringsUtils.hasText(value) para reemplazar
		 *value.isEmpty() || value.isBlank()*/
		
		if (value == null || value.isEmpty() || value.isBlank()) {
			return false;
		}
		return true;
	}

}
