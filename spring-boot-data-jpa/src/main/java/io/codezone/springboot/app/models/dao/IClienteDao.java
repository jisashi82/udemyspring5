package io.codezone.springboot.app.models.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import io.codezone.springboot.app.models.entity.Cliente;

public interface IClienteDao extends PagingAndSortingRepository<Cliente, Long> {

    /*mejorando la consulta con fetch y left join para visualizar las facturas de un cliente */
    @Query("select c from Cliente c left join fetch c.facturas f where c.id=?1")
    public Cliente fetchByIdWithFacturas(Long id);

    public void save(Cliente cliente);

    public Optional<Cliente> findById(Long id);

    public void deleteById(Long id);
	
}
