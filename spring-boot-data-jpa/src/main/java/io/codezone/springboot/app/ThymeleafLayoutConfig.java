package io.codezone.springboot.app;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//import nz.net.ultraq.thymeleaf.LayoutDialect;
import nz.net.ultraq.thymeleaf.layoutdialect.*;;

@Configuration
public class ThymeleafLayoutConfig {
	
	@Bean
	public LayoutDialect layoutDialect() {
	  return new LayoutDialect();
	}

}
