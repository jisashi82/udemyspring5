package io.codezone.springboot.app.models.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import io.codezone.springboot.app.models.entity.Factura;

public interface IFacturaDao extends CrudRepository<Factura, Long>{

    /*implementando metodo para optimizar la consulta de las facturas con sus items mediante
     * JPQL ya que por cada factura tiene muchas items genera hasta 7 consultas a la base de datos*/

    @Query("select f from Factura f join fetch f.cliente c join fetch f.items l join fetch l.producto where f.id = ?1")
    public Factura fecthByIdWithClienteWithItemFacturaWithProducto(Long id);
    
}
