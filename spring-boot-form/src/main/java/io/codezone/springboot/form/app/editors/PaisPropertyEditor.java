package io.codezone.springboot.form.app.editors;

import java.beans.PropertyEditorSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import io.codezone.springboot.form.app.services.IPaisService;

@Component
public class PaisPropertyEditor extends PropertyEditorSupport {

	@Autowired
	private IPaisService paisService;

	@Override
	public void setAsText(String id) throws IllegalArgumentException {
		try {
			this.setValue(paisService.obtenerPorId(Integer.parseInt(id)));
		} catch (NumberFormatException e) {
			setValue(null);
		}
	}

}
