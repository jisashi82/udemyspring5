package io.codezone.springboot.form.app.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import io.codezone.springboot.form.app.models.Role;

@Service
public class RoleServiceImpl implements IRoleService{
	
	private List<Role> roles;
	
	

	public RoleServiceImpl() {
		this.roles=new ArrayList<>();
		this.roles.add(new Role(1,"Administrador", "ROLE_ADMIN"));
		this.roles.add(new Role(2,"Usuario", "ROLE_USER"));
		this.roles.add(new Role(3,"Invitado", "ROLE_GUEST"));
	}

	@Override
	public List<Role> listar() {
		return this.roles;
	}

	@Override
	public Role obtenerPorId(Integer id) {
		Role resultado= null;
		for (Role role : roles) {
			if(role.getId() == id) {
				resultado = role;
				break;
			}
		}
		return resultado;
	}

}
