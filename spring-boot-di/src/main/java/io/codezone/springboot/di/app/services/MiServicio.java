package io.codezone.springboot.di.app.services;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Primary
@Service("miServicioSimple")
public class MiServicio implements IServicio{

	@Override
	public String operacion() {
		return "Ejecutando algun proceso importante! Simple...";		
	}
}
