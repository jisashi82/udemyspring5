package io.codezone.springboot.form.app.services;

import java.util.List;

import io.codezone.springboot.form.app.models.Pais;

public interface IPaisService {
	
	public List<Pais> listar();
	public Pais obtenerPorId(Integer id);

}
