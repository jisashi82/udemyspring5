package io.codezone.springboot.error.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import io.codezone.springboot.error.app.models.domain.Usuario;
import io.codezone.springboot.error.app.services.IUsuarioService;

@Controller
public class AppController {
	
	@Autowired
	private IUsuarioService usuarioService;
	
	@SuppressWarnings("unused")
	@GetMapping("/index")
	public String index() {
		//Integer valor= 100/0;
		Integer valor=Integer.parseInt("10xaaaa");
		return "index";
	}
	
	@GetMapping("/ver/{id}")
	public String ver(@PathVariable Integer id, Model model) {
		Usuario usuario=usuarioService.obtenerPorId(id);
				
		model.addAttribute("titulo", "Busqueda de usuario".concat(usuario.getNombre()));
		model.addAttribute("usuario", usuario);
		return "ver";
	}

}
