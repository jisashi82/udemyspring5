package io.codezone.springboot.form.app.services;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

import io.codezone.springboot.form.app.models.Pais;

@Service
public class PaisServiceImpl implements IPaisService {
	
	private List<Pais> lista;

	public PaisServiceImpl() {
		this.lista=Arrays.asList(new Pais(1, "ES", "España"), new Pais(2, "MX", "Mexico"), new Pais(3, "CL", "Chile"),
				new Pais(4, "VE", "Venezuela"), new Pais(5, "CO", "Colombia"), new Pais(6, "AR", "Argentina"));
	}

	@Override
	public List<Pais> listar() {
		return lista;
	}

	@Override
	public Pais obtenerPorId(Integer id) {
		Pais pais=null;
		
		for(Pais p: this.lista) {
			if(p.getId() == id) {
				pais = p;
				break;
			}
		}
		
		return pais;
	}
	
	

}
