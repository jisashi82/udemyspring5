package io.codezone.springboot.di.app;

import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.codezone.springboot.di.app.models.ItemFactura;
import io.codezone.springboot.di.app.models.Producto;

@Configuration
public class AppConfig {

	@Bean("itemsFactura")
	public List<ItemFactura> registrarItems() {
		Producto producto1=new Producto("Camara Sony", 100);
		Producto producto2=new Producto("Bicicleta Bianchi aro 25", 200);
		Producto producto3=new Producto("Latop Lenovo Ideapad 14", 250);
		
		ItemFactura linea1= new ItemFactura(producto1, 2);
		ItemFactura linea2= new ItemFactura(producto3, 2);
		ItemFactura linea3= new ItemFactura(producto2, 2);
		
		return Arrays.asList(linea1, linea2, linea3);
		
	}
}
