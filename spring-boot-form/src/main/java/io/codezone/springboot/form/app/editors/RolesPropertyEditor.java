package io.codezone.springboot.form.app.editors;

import java.beans.PropertyEditorSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import io.codezone.springboot.form.app.services.IRoleService;

@Component
public class RolesPropertyEditor extends PropertyEditorSupport{
	
	@Autowired
	private IRoleService roleService;

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		try {
			setValue(roleService.obtenerPorId(Integer.parseInt(text)));
		} catch (NumberFormatException e) {
			setValue(null);
		}
	}
}
